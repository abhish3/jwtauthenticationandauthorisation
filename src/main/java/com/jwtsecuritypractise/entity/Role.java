package com.jwtsecuritypractise.entity;

public enum Role {
    ADMIN,
    USER
}
