package com.jwtsecuritypractise.service;

import com.jwtsecuritypractise.dto.JwtAuthenticationResponse;
import com.jwtsecuritypractise.dto.RefreshTokenRequest;
import com.jwtsecuritypractise.dto.SignUpRequest;
import com.jwtsecuritypractise.dto.SigninRequest;
import com.jwtsecuritypractise.entity.User;

public interface AuthenticationService {
    User signUp(SignUpRequest signUpRequest);
    JwtAuthenticationResponse signIn(SigninRequest signinRequest);
    JwtAuthenticationResponse refreshToken(RefreshTokenRequest refreshTokenRequest);

}
