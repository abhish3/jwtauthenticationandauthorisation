package com.jwtsecuritypractise.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {
    public ResponseEntity<String> userPage() {
        return ResponseEntity.ok("User Page");
    }
}
