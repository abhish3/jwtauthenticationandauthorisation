package com.jwtsecuritypractise.dto;

import lombok.Data;

@Data
public class RefreshTokenRequest {

    private String token;
}
